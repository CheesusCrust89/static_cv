---
layout: default
title: cv
---
# dr. Csaba Almási
Cloud infrastructure and devops engineer, kubernetes administrator.
Check out my code samples at my [Gitlab repo](https://gitlab.com/CheesusCrust89/terraform_azure)
and my [linkedIn](https://www.linkedin.com/in/csaba-almasi/)

## Currently

Cloud solution architect @Microsoft.

### Specialized in

* Infrastructure as code with Terraform
* CI/CD pipelines for both IaC and workloads
* Kubernetes cluster hardening in multiple flavours(GKE, AKS and RedHat Openshift)

## Occupation

`Nov 2020 - present`
__Cloud Solution Architect - Microsoft__, Oslo - Norway

* Designing and implementing highly available and secure infrastructure and kubernetes clusters in Azure and in multi-cloud environments
* Guiding customers' cloud adoption journey, both in technical and organizational health terms
* Technical upskilling of customers and implementing partners
* Creating delivery timelines, cost estimates and business value analyses

`March 2020 - Nov 2020 `
__Cloud infrastructure engineer - Cognite__, Oslo - Norway

* Provisioning and maintaining kubernetes infrastructure (GKE, AKS, Openshift) using Terraform + kustomize & kubecfg
* Developing custom constrollers for third party applications in GO (Hashicorp Vault)
* Maintaining CI/CD pipelines (GH actions and Spinnaker)
* Provisioning and maintaining storage clusters (Rook + Ceph)

`July 2019 - March 2020`
__Solution architect - Cognite__, Oslo - Norway

* Designing and implementing ETL pipelines and integration middleware
* GO, nodeJS development
* Jenkins and Terraform in the provisioning space
* Azure functions, Databaricks, GCP cloud functions and dataflow + Airflow on AKS for runtime
* Day-to-day tasks include technical consultancy and workshops for Clients, mentoring juniors and interns

`Feb 2018 - June 2019`
__Software Engineer(senior from Dec 2018) - Accenture__, Budapest - Hungary && Bratislava - Slovakia

* Code reviews, mentoring, architecture design and general implementation tasks, along with sprint management and technical planning. Stack includes: NodeJS, golang, AWS.
* Advanced Technology Platform, main focus is on developing and deploying distributed ledger technology implementations(Hyperledger Fabric, Ethereum Solidity)

`Mar 2017 - Feb 2018`
__Junior Java Developer - Synoa GmbH__, Budapest - Hungary && Frankfurt - Germany

* Mainly middleware integration services for German clients using microservice architecture with the tech stack of Apache Camel, MongoDb, AMQ and Spring-Boot.

## Education

`Oct 2016 - March 2017`
__[GreenFox Academy](https://www.greenfoxacademy.com/) - Java developer bootcamp__

* Zerda Raptors class
* Focus on backend, JAVA fundamentals with security

`2007-2015`
__Eötvös Loránd University - Faculty of Law, Budapest__

* Grade: Cum laude
* Activities and societies: Bibó István college for advanced legal studies
* Student teacher's assistant - Department of Civil Procedure

`2011 - 2012`
__Tilburg University__

* Erasmus program, Faculty of Law
* Focus areas include: cybercrime, EU trade law
<!-- ### Footer

Last updated: Aug 2022 -->


